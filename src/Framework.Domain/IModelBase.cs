namespace Framework.Domain
{
    /// <summary>
    /// The interface for all domain models.
    /// </summary>
    public interface IModelBase
    {
        /// <summary>
        /// Gets or sets a flag indicating if the model has been modified.
        /// </summary>
        bool IsDirty { get; set; }
        
        /// <summary>
        /// Gets or sets a flag indicating if the events are suspended.
        /// </summary>
        bool AreEventsSuspended { get; set; }
        
        /// <summary>
        /// Resets the IsDirty flag.
        /// </summary>
        void ResetIsDirty();
    }
}