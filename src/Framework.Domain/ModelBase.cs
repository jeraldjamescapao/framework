﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Framework.Domain
{
    /// <summary>
    /// Base class for all domain models.
    /// </summary>
    [DataContract]
    [Serializable]
    public abstract class ModelBase : IModelBase, INotifyPropertyChanging, INotifyPropertyChanged, IDisposable
    {
        #region Ctor

        protected ModelBase()
        {
            IsDirty = false;
            AreEventsSuspended = false;
        }
        
        #endregion
        
        #region Properties & Corresponding Fields
        
        /// <summary>
        /// Gets or sets a flag indicating if the model has been modified.
        /// </summary>
        [DataMember]
        public bool IsDirty
        {
            get => _isDirty;
            set
            {
                if (_isDirty == value) return;
                _isDirty = value;
                OnSetProperty(nameof(IsDirty));
            }
        }
        private bool _isDirty;
        
        /// <summary>
        /// Gets or sets a flag indicating if the events are suspended.
        /// </summary>
        [DataMember]
        public bool AreEventsSuspended
        {
            get => _areEventsSuspended;
            set
            {
                if (_areEventsSuspended == value) return;
                _areEventsSuspended = value;
                OnSetProperty(nameof(AreEventsSuspended));
            }
        }
        private bool _areEventsSuspended;
        
        #endregion
        
        #region Virtual Methods
        
        /// <summary>
        /// Resets the is dirty flag.
        /// </summary>
        public void ResetIsDirty()
        {
            IsDirty = false;
        }
        
        /// <summary>
        /// Standard member initialization routine.
        /// </summary>
        protected virtual void InitializeMembers() { }

        /// <summary>
        /// Call on set property to raise events.
        /// </summary>
        /// <param name="propertyName">The name of the property.</param>
        private void OnSetProperty(string propertyName)
        {
            OnPropertyChanging(propertyName);
            OnPropertyChanged(propertyName);
        }
        
        /// <summary>
        /// Standard member finalization routine.
        /// </summary>
        private void FinalizeMembers()
        {
            // Allows sub classes to override if needed.
        }
        
        #endregion
        
        #region INotifyPropertyChanging Members
        
        /// <summary>
        /// Property changing event.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;
        
        /// <summary>
        /// Gets the property changing event handler.
        /// </summary>
        internal PropertyChangingEventHandler PropertyChangingEventHandler => PropertyChanging;
        
        /// <summary>
        /// Call the OnPropertyChanging method to raise the event.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        private void OnPropertyChanging(string propertyName)
        {
            if (AreEventsSuspended) return;
            if (propertyName != nameof(IsDirty))
            {
                IsDirty = true;
            }
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(propertyName));
        }

        #endregion
        
        #region INotifyPropertyChanged Members
        
        /// <summary>
        /// Property changed event.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        
        /// <summary>
        /// Gets the property changed event handler.
        /// </summary>
        internal PropertyChangedEventHandler PropertyChangedEventHandler => PropertyChanged;
        
        /// <summary>
        /// Call the OnPropertyChanged method to raise the event.
        /// </summary>
        /// <param name="propertyName">The property name.</param>
        private void OnPropertyChanged(string propertyName)
        {
            if (AreEventsSuspended == false)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
        
        #region IDisposable Members
        
        /// <summary>
        /// Clear memory pointers.
        /// </summary>
        /// <remarks>Dispose() calls Dispose(true).</remarks>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        /// <summary>
        /// Dispose handling.
        /// </summary>
        /// <param name="disposing">Flag indicating if disposing must be done.</param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                FinalizeMembers(); // Free managed resources
            }
            // Free native resources if there are any.
        }
        
        #endregion
    }
}