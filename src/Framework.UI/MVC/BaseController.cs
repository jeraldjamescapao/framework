﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Core.Caching;
using Framework.Core.Configuration;
using Framework.Core.Exceptions;
using Framework.Core.Logging;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Framework.UI.MVC
{
    /// <summary>
    /// The base controller class handling.
    /// </summary>
    public abstract class BaseController : Controller
    {
        #region Ctor

        protected BaseController(IConfiguration config, ILogger<BaseController> logger, IMemoryCache memoryCache)
        {
            ConfigManager = new ConfigurationManager(config);
            LogManager = new LoggingManager<BaseController>(logger);
            CacheManager = new CachingManager(memoryCache);
            ViewBag.LoggedAppUser = ""; // ToDo: Get the value through caching!
        }

        #endregion

        #region Properties
        
        /// <summary>
        /// The configuration manager to use.
        /// </summary>
        protected ConfigurationManager ConfigManager { get;  }
        
        /// <summary>
        /// The logger to use.
        /// </summary>
        protected LoggingManager<BaseController> LogManager { get; }
        
        /// <summary>
        /// The cache manager to use.
        /// </summary>
        protected CachingManager CacheManager { get; }

        /// <summary>
        /// Gets the main header title used to show in view.
        /// </summary>
        protected abstract string MainHeaderTitle { get; }

        /// <summary>
        /// Gets or sets the site's supported cultures.
        /// </summary>
        protected List<string> SupportedCultures { get; set; }
        
        /// <summary>
        /// Gets a flag indicating if a general error occurred.
        /// </summary>
        protected bool HasGeneralError => string.IsNullOrWhiteSpace(ViewBag.GeneralError) == false;

        /// <summary>
        /// Gets a flag indicating if there's a warning.
        /// </summary>
        protected bool HasWarning => string.IsNullOrWhiteSpace(ViewBag.GeneralWarning) == false;

        /// <summary>
        /// Gets a flag indicating if an edit error occurred.
        /// </summary>
        protected bool HasEditError => string.IsNullOrWhiteSpace(ViewBag.EditError) == false;
        
        /// <summary>
        /// Gets a flag indicating whether an operation succeeded.
        /// </summary>
        protected bool OperationSucceeded => HasGeneralError == false && HasEditError == false;

        #endregion

        #region Overrides

        /// <summary>
        /// Releases unmanaged resources and optionally releases managed resources.
        /// </summary>
        /// <param name="disposing">Flag indicating to release both managed and unmanaged resources or only unmanaged.</param>
        protected override void Dispose(bool disposing)
        {
            FinalizeMembers();
            base.Dispose(disposing);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Standard member initialization routine.
        /// </summary>
        protected virtual void InitializeMembers()
        {
            // Allows subclasses to override. 
        }
        
        /// <summary>
        /// Standard member finalization routine.
        /// </summary>
        protected virtual void FinalizeMembers()
        { 
            // Allows subclasses to override.
        }
        
        /// <summary>
        /// Gets the current session Id.
        /// </summary>
        protected string GetCurrentSessionId()
        {
            return HttpContext.Session?.Id;
        }

        #region Safe execute void

        /// <summary>
        /// Safely executes a method.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        protected void SafeExecute(Action method)
        {
            try
            {
                method();
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param">The parameter to use.</param>
        /// <typeparam name="T">The type of the parameter.</typeparam>
        protected void SafeExecute<T>(Action<T> method, T param)
        {
            try
            {
                method(param);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        protected void SafeExecute<T1, T2>(Action<T1, T2> method, T1 param1, T2 param2)
        {
            try
            {
                method(param1, param2);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        protected void SafeExecute<T1, T2, T3>(Action<T1, T2, T3> method, T1 param1, T2 param2, T3 param3)
        {
            try
            {
                method(param1, param2, param3);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }

        #endregion
        
        #region Safe execute with return value
        
        /// <summary>
        /// Safely executes a method which returns a value.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecute<T>(Func<T> method)
        {
            try
            {
                return method();
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecute<T1, T>(Func<T1, T> method, T1 param1)
        {
            try
            {
                return method(param1);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecute<T1, T2, T>(Func<T1, T2, T> method, T1 param1, T2 param2)
        {
            try
            {
                return method(param1, param2);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecute<T1, T2, T3, T>(Func<T1, T2, T3, T> method, T1 param1, T2 param2, T3 param3)
        {
            try
            {
                return method(param1, param2, param3);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        #endregion

        #region Safe execute with ViewResult return value.

        /// <summary>
        /// Safely executes a method which returns a view (ViewResult).
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecuteViewResult<T>(Func<T> method) where T : ViewResult
        {
            try
            {
                return method();
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return (T)View("Error");
        }

        #endregion
        
        #region Safe execute with PartialViewResult return value
        
        /// <summary>
        /// Safely executes a method which returns a partial view.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected T SafeExecutePartialViewResult<T>(Func<T> method) where T : PartialViewResult
        {
            try
            {
                return method();
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        #endregion
        
        #region Safe execute asynchronously a method that returns a Task 
        
        /// <summary>
        /// Safely executes a method asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        protected async Task SafeExecuteAsync(Action method)
        {
            try
            {
                await Task.Factory.StartNew(method).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param">The parameter to use.</param>
        /// <typeparam name="T">The type of the parameter.</typeparam>
        protected async Task SafeExecuteAsync<T>(Action<T> method, T param)
        {
            try
            {
                await Task.Factory.StartNew(() => method(param)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        protected async Task SafeExecuteAsync<T1, T2>(Action<T1, T2> method, T1 param1, T2 param2)
        {
            try
            {
                await Task.Factory.StartNew(() => method(param1, param2)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        /// <summary>
        /// Safely executes a method asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        protected async Task SafeExecuteAsync<T1, T2, T3>(Action<T1, T2, T3> method, T1 param1, T2 param2, T3 param3)
        {
            try
            {
                await Task.Factory.StartNew(() => method(param1, param2, param3)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
        }
        
        #endregion
        
        #region Safe execute asynchronously with return value
        
        /// <summary>
        /// Safely executes a method which returns a value asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteAsync<T>(Func<T> method)
        {
            try
            {
                return await Task.Factory.StartNew(method).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteAsync<T1, T>(Func<T1, T> method, T1 param1)
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteAsync<T1, T2, T>(Func<T1, T2, T> method, T1 param1, T2 param2)
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1, param2)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a value asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteAsync<T1, T2, T3, T>(Func<T1, T2, T3, T> method, T1 param1, T2 param2, T3 param3)
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1, param2, param3))
                    .ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        #endregion
        
        #region Safe execute asynchronously with ViewResult return value
        
        /// <summary>
        /// Safely executes a method which returns a view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteViewResultAsync<T>(Func<T> method) where T : ViewResult
        {
            try
            {
                return await Task.Factory.StartNew(method).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return (T)View("Error");
        }
        
        /// <summary>
        /// Safely executes a method which returns a view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteViewResultAsync<T1, T>(Func<T1, T> method, T1 param1) where T : ViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return (T)View("Error");
        }
        
        /// <summary>
        /// Safely executes a method which returns a view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteViewResultAsync<T1, T2, T>(Func<T1, T2, T> method, T1 param1, T2 param2) where T : ViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1, param2)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return (T)View("Error");
        }
        
        /// <summary>
        /// Safely executes a method which returns a view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecuteViewResultAsync<T1, T2, T3, T>(Func<T1, T2, T3, T> method, T1 param1, T2 param2, T3 param3) 
            where T : ViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => 
                    method(param1, param2, param3)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return (T)View("Error");
        }
        
        #endregion
        
        #region Safe execute asynchronously with PartialViewResult return value
        
        /// <summary>
        /// Safely executes a method which returns a partial view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecutePartialViewResultAsync<T>(Func<T> method) where T : PartialViewResult
        {
            try
            {
                return await Task.Factory.StartNew(method).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a partial view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecutePartialViewResultAsync<T1, T>(Func<T1, T> method, T1 param1) where T : PartialViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a partial view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecutePartialViewResultAsync<T1, T2, T>(Func<T1, T2, T> method, T1 param1, T2 param2) 
            where T : PartialViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => method(param1, param2)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        /// <summary>
        /// Safely executes a method which returns a partial view asynchronously.
        /// </summary>
        /// <param name="method">The method to execute.</param>
        /// <param name="param1">The parameter 1 to use.</param>
        /// <param name="param2">The parameter 2 to use.</param>
        /// <param name="param3">The parameter 3 to use.</param>
        /// <typeparam name="T1">The type of the parameter 1.</typeparam>
        /// <typeparam name="T2">The type of the parameter 2.</typeparam>
        /// <typeparam name="T3">The type of the parameter 3.</typeparam>
        /// <typeparam name="T">The type to return.</typeparam>
        protected async Task<T> SafeExecutePartialViewResultAsync<T1, T2, T3, T>(Func<T1, T2, T3, T> method, T1 param1, T2 param2, T3 param3)
            where T : PartialViewResult
        {
            try
            {
                return await Task.Factory.StartNew(() => 
                    method(param1, param2, param3)).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SetGeneralErrorText(ExceptionUtility.GetInnerMessage(ex));
            }
            return default;
        }
        
        #endregion
        
        #region Information

        /// <summary>
        /// Sets the general error text.
        /// </summary>
        /// <param name="message">The general error message to set.</param>
        protected void SetGeneralErrorText(string message)
        {
            ViewBag.GeneralError = System.Net.WebUtility.HtmlEncode(message);
        }
        
        /// <summary>
        /// Sets the warning error text.
        /// </summary>
        /// <param name="message">The warning message to set.</param>
        protected void SetWarningErrorText(string message)
        {
            ViewBag.EditError = System.Net.WebUtility.HtmlEncode(message);
        }
        
        /// <summary>
        /// Sets the edit error text.
        /// </summary>
        /// <param name="message">The edit error message to set.</param>
        protected void SetEditErrorText(string message)
        {
            ViewBag.EditError = System.Net.WebUtility.HtmlEncode(message);
        }
        
        /// <summary>
        /// Sets the model state errors.
        /// </summary>
        protected void SetModelStateErrors()
        {
            // Need more test to verify Aggregate function.
            var error = "Please correct validation errors before saving changes."; // ToDo: resources - Validation.ValidationErrorMessage;
            error = ModelState.Values.Aggregate(error, (current1, value) => 
                value.Errors.Aggregate(current1, (current, err) => current + $"{Environment.NewLine}{err.ErrorMessage}"));
            SetGeneralErrorText(error);
        }
        
        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Adds an error in the model state.
        /// </summary>
        /// <param name="errorText">The error text to add.</param>
        protected void AddModelStateError(string errorText)
        {
            ModelState.AddModelError(string.Empty, errorText);
            SetGeneralErrorText(errorText);
        }

        #endregion
    }
}