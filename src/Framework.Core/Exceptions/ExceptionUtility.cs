using System;

namespace Framework.Core.Exceptions
{
    /// <summary>
    /// Encapsulates static utility methods for exceptions handling.
    /// </summary>
    public static class ExceptionUtility
    {
        #region Information

        /// <summary>
        /// Get the inner message from an exception if exists.
        /// </summary>
        /// <param name="ex">The exception to use.</param>
        public static string GetInnerMessage(Exception ex)
        {
            var result = default(string);
            if (ex.InnerException != null)
            {
                result = GetInnerMessage(ex.InnerException);
            }
            // Remove redundant messages.
            if (string.IsNullOrWhiteSpace(result))
            {
                result = ex.Message;
            }
            else if (!result.StartsWith(ex.Message))
            {
                result = ex.Message + Environment.NewLine + result;
            }
            return result;
        }

        #endregion
    }
}