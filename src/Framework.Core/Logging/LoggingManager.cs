using Microsoft.Extensions.Logging;
using System;

namespace Framework.Core.Logging
{
    /// <summary>
    /// The generic logging manager class handling.
    /// </summary>
    public class LoggingManager<T>
    {
        #region Fields

        private readonly ILogger<T> _logger;

        #endregion

        #region Ctor

        public LoggingManager(ILogger<T> logger)
        {
            _logger = logger;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Logs information message to the underlying loggers.
        /// </summary>
        /// <param name="message">The information message to log.</param>
        public void LogInformation(string message)
        {
            _logger.LogInformation(message);
        }

        /// <summary>
        /// Logs trace message to the underlying loggers.
        /// </summary>
        /// <param name="message">The trace message to log.</param>
        public void LogTrace(string message)
        {
            _logger.LogTrace((message));
        }
        
        /// <summary>
        /// Logs warning message to the underlying loggers.
        /// </summary>
        /// <param name="message">The warning message to log.</param>
        public void LogWarning(string message)
        {
            _logger.LogWarning((message));
        }

        /// <summary>
        /// Logs warning message to the underlying loggers.
        /// </summary>
        /// <param name="message">The warning message to log.</param>
        /// <param name="ex">The exception to log.</param>
        public void LogWarning(string message, Exception ex)
        {
            _logger.LogWarning(ex, message);
        }
        
        /// <summary>
        /// Logs error message to the underlying loggers.
        /// </summary>
        /// <param name="message">The error message to log.</param>
        public void LogError(string message)
        {
            _logger.LogError((message));
        }
        
        /// <summary>
        /// Logs error message to the underlying loggers.
        /// </summary>
        /// <param name="message">The error message to log.</param>
        /// <param name="ex">The exception to log.</param>
        public void LogError(string message, Exception ex)
        {
            _logger.LogError(ex, message);
        }
        
        /// <summary>
        /// Logs error message to the underlying loggers.
        /// </summary>
        /// <param name="message">The error message to log.</param>
        /// <param name="callSite">The call site to log.</param>
        /// <param name="ex">The exception to log.</param>
        public void LogError(string message, string callSite, Exception ex)
        {
            message = !string.IsNullOrWhiteSpace(callSite) ? $"{Environment.NewLine} Call site: {callSite}" : message;
            _logger.LogError(ex, message);
        }

        #endregion
    }
}