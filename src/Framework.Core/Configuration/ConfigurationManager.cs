﻿using System;
using Microsoft.Extensions.Configuration;

namespace Framework.Core.Configuration
{
    /// <summary>
    /// The manager class for application configuration handling.
    /// </summary>
    public class ConfigurationManager
    {
        #region Fields

        private readonly IConfiguration _configuration;
        private string _defaultApplicationCulture;
        private (string ConnectionString, string ProviderType) _defaultDatabaseConnection;

        #endregion
        
        #region Ctor

        public ConfigurationManager(IConfiguration iConfig)
        {
            _configuration = iConfig;
        }
        
        #endregion
        
        #region Connection String

        /// <summary>
        /// Gets the default connection string and provider type from the configuration file.
        /// </summary>
        /// <returns>A tuple of connection string and provider type.</returns>
        public (string ConnectionString, string ProviderType) GetDefaultConnectionString()
        {
            if (string.IsNullOrWhiteSpace(_defaultDatabaseConnection.ConnectionString))
            {
                _defaultDatabaseConnection = GetConnectionString(ConfigurationKeyType.DefaultConnectionString.ToString());
            }
            return _defaultDatabaseConnection;
        }
        
        /// <summary>
        /// Gets the connection string and the provider type from the configuration file according to the given name.
        /// </summary>
        /// <param name="connectionStringName">The given connection string name that specifies which connection string to retrieve.</param>
        /// <returns>A tuple of connection string and provider type.</returns>
        public (string ConnectionString, string ProviderType) GetConnectionString(string connectionStringName)
        {
            string connectionString = default;
            string providerType = default;
            var connectionStringsSections = _configuration.GetSection(ConfigurationKeyType.ConnectionStrings.ToString()).GetChildren();
            foreach (var section in connectionStringsSections)
            {
                if (!string.Equals(section.Key, connectionStringName, StringComparison.CurrentCultureIgnoreCase)) continue;
                connectionString = section.GetSection(ConfigurationKeyType.ConnectionString.ToString()).Value;
                providerType = section.GetSection(ConfigurationKeyType.ProviderType.ToString()).Value;
                break;
            }
            return (connectionString, providerType);
        }

        #endregion
        
        #region Culture

        /// <summary>
        /// Gets the default application culture.
        /// </summary>
        /// <returns>The default culture.</returns>
        public string GetDefaultApplicationCulture()
        {
            if (!string.IsNullOrWhiteSpace(_defaultApplicationCulture)) return _defaultApplicationCulture;
            _defaultApplicationCulture = "en-US";
            var defaultApplicationCulture = GetConfigurationValue(ConfigurationKeyType.DefaultApplicationCulture.ToString());
            if (!string.IsNullOrWhiteSpace(defaultApplicationCulture))
            {
                _defaultApplicationCulture = defaultApplicationCulture;
            }
            return _defaultApplicationCulture;
        }
        
        #endregion
        
        #region Section

        /// <summary>
        /// Gets the configuration value according to the given section name.
        /// </summary>
        /// <param name="sectionName">The section to use.</param>
        /// <returns></returns>
        public string GetConfigurationValue(string sectionName)
        {
            return _configuration.GetSection(sectionName).Value;
        }
        
        #endregion
    }
}
