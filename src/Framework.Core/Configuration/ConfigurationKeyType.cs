﻿namespace Framework.Core.Configuration
{
    /// <summary>The configuration key types used in the application.</summary>
    public enum ConfigurationKeyType
    {
        ConnectionString,
        ConnectionStrings,
        DefaultApplicationCulture,
        DefaultConnectionString,
        //DefaultEmailSender,
        //EmailRedirectionAddress,
        //GoogleApiKey,
        ProviderType
    }
}
