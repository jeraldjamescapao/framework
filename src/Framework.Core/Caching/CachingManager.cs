using System;
using Microsoft.Extensions.Caching.Memory;

namespace Framework.Core.Caching
{
    /// <summary>
    /// The caching manager class handling.
    /// </summary>
    public class CachingManager
    {
        #region Fields

        private readonly IMemoryCache _memoryCache;

        #endregion
        
        #region Ctor

        public CachingManager(IMemoryCache memoryMemoryCache)
        {
            _memoryCache = memoryMemoryCache;
        }
        
        #endregion
        
        #region Constants

        private const int DEFAULT_CACHE_TIME_IN_MINUTES = 30;
        private const CacheItemPriority DEFAULT_CACHE_ITEM_PRIORITY = CacheItemPriority.Normal;

        #endregion
        
        #region Methods

        #region Get

        /// <summary>
        /// Gets the item from the memory cache based on the key name.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        /// <remarks>
        /// Call this function if you are sure that there is a value on a certain cache key.
        /// If there's none, the default value of a type is returned.
        /// </remarks>
        public T GetItemFromCache<T>(string cacheKeyName)
        {
            TryToGetItemFromCache(cacheKeyName, out T value);
            return value;
        }

        /// <summary>
        /// A generic method or setting and getting items to and from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="defaultItemValue">The default item value to set if cache item and value don't exist yet.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        /// <typeparam name="T"></typeparam>
        public T GetItemFromCache<T>(string cacheKeyName, T defaultItemValue, int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES,
            CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (TryToGetItemFromCache(cacheKeyName, out T value)) return value;
            value = defaultItemValue;
            SetItemToCache(cacheKeyName, defaultItemValue, cacheTimeInMinutes, priority);
            return value;
        }

        /// <summary>
        /// A generic method or setting and getting items to and from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="itemSettingFunction">A function (w/o parameters) to call if the item isn't in the cache and returns the default value.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for.</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        public T GetItemFromCache<T>(string cacheKeyName, Func<T> itemSettingFunction, int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES, 
            CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (TryToGetItemFromCache(cacheKeyName, out T value)) return value;
            var defaultItemValue = itemSettingFunction();
            value = defaultItemValue;
            SetItemToCache(cacheKeyName, defaultItemValue, cacheTimeInMinutes, priority);
            return value;
        }

        /// <summary>
        /// A generic method or setting and getting items to and from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="itemSettingFunction">A function (w/ 1 parameter) to call if the item isn't in the cache and returns the default value.</param>
        /// <param name="param">The passed function's parameter.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for.</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        /// <typeparam name="T1">The type of the parameter.</typeparam>
        public T GetItemFromCache<T, T1>(string cacheKeyName, Func<T1, T> itemSettingFunction, T1 param, 
            int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES, CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (TryToGetItemFromCache(cacheKeyName, out T value)) return value;
            var defaultItemValue = itemSettingFunction(param);
            value = defaultItemValue;
            SetItemToCache(cacheKeyName, defaultItemValue, cacheTimeInMinutes, priority);
            return value;
        }

        /// <summary>
        /// A generic method or setting and getting items to and from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="itemSettingFunction">A function (w/ 2 parameters) to call if the item isn't in the cache and returns the default value.</param>
        /// <param name="param1">The passed function's first parameter.</param>
        /// <param name="param2">The passed function's second parameter.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for.</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        public T GetItemFromCache<T, T1, T2>(string cacheKeyName, Func<T1, T2, T> itemSettingFunction, T1 param1, T2 param2, 
            int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES, CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (TryToGetItemFromCache(cacheKeyName, out T value)) return value;
            var defaultItemValue = itemSettingFunction(param1, param2);
            value = defaultItemValue;
            SetItemToCache(cacheKeyName, defaultItemValue, cacheTimeInMinutes, priority);
            return value;
        }

        /// <summary>
        /// A generic method or setting and getting items to and from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="itemSettingFunction">A function (w/ 3 parameters) to call if the item isn't in the cache and returns the default value.</param>
        /// <param name="param1">The passed function's first parameter.</param>
        /// <param name="param2">The passed function's second parameter.</param>
        /// <param name="param3">The passed function's third parameter.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for.</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        /// <typeparam name="T1">The type of the first parameter.</typeparam>
        /// <typeparam name="T2">The type of the second parameter.</typeparam>
        /// <typeparam name="T3">the type of the third parameter.</typeparam>
        public T GetItemFromCache<T, T1, T2, T3>(string cacheKeyName, Func<T1, T2, T3, T> itemSettingFunction, T1 param1, T2 param2, T3 param3,
            int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES, CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (TryToGetItemFromCache(cacheKeyName, out T value)) return value;
            var defaultItemValue = itemSettingFunction(param1, param2, param3);
            value = defaultItemValue;
            SetItemToCache(cacheKeyName, defaultItemValue, cacheTimeInMinutes, priority);
            return value;
        }
        
        /// <summary>
        /// Tries to get item from the memory cache based on the key name.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="value">The value to be returned if the object exists on the cache.</param>
        /// <typeparam name="T">The type of object/item to be returned.</typeparam>
        public bool TryToGetItemFromCache<T>(string cacheKeyName, out T value)
        {
            value = default;
            return !string.IsNullOrWhiteSpace(cacheKeyName) && _memoryCache.TryGetValue(cacheKeyName, out value);
        }

        #endregion

        #region Set

        /// <summary>
        /// Sets the item to the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        /// <param name="value">The value to set.</param>
        /// <param name="cacheTimeInMinutes">Determines how long to cache this item for.</param>
        /// <param name="priority">Specifies how this item is prioritized for preservation during a memory pressure triggered cleanup.</param>
        public void SetItemToCache(string cacheKeyName, object value, int cacheTimeInMinutes = DEFAULT_CACHE_TIME_IN_MINUTES,
            CacheItemPriority priority = DEFAULT_CACHE_ITEM_PRIORITY)
        {
            if (value != null)
            {
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetPriority(priority)
                    .SetSlidingExpiration(TimeSpan.FromMinutes(cacheTimeInMinutes));
                _memoryCache.Set(cacheKeyName, value, cacheEntryOptions);
            }
            else
            {
                RemoveItemFromCache(cacheKeyName);
            }
        }

        #endregion

        #region Remove

        /// <summary>
        /// Removes the item from the memory cache.
        /// </summary>
        /// <param name="cacheKeyName">The cache entry key name.</param>
        public void RemoveItemFromCache(string cacheKeyName)
        {
            if (!string.IsNullOrWhiteSpace(cacheKeyName))
            {
                _memoryCache.Remove(cacheKeyName);
            }
        }

        #endregion

        #endregion
    }
}