﻿using System;
using System.IO;
using Framework.Core.Configuration;
using Microsoft.Extensions.Configuration;

namespace Framework.Core.Test
{
    internal static class Program
    {
        private static IConfiguration Configuration { get; set; }
        public static void Main(string[] args)
        {
            // Note: a Good example of appSettings.json is found in /bin/debug/netcoreapp3.1/
            var directory = Path.Combine(AppContext.BaseDirectory);
            var builder = new ConfigurationBuilder().SetBasePath(directory).AddJsonFile("appsettings.json", true);
            var configuration = builder.Build();
            var configManager = new ConfigurationManager(configuration);
            // var (connectionString, providerType) = configManager.GetConnectionString(ConnectionStringNameKeyType.SqlServer.ToString());
            var (connectionString, providerType) = configManager.GetDefaultConnectionString();
            Console.WriteLine($@"Connection String: {connectionString}");
            Console.WriteLine($@"Provider Type: {providerType}");
            Console.WriteLine($@"Default Culture: {configManager.GetDefaultApplicationCulture()}");
            var random = configManager.GetConfigurationValue("Love");
            Console.WriteLine($@"Random: {random}");
        }
    }
}